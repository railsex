RAILSEX_MAPPING = { 'console'         => 'railties/console', 
                    'lazy_attributes' => 'activerecord/changed',
                    'respond_to'      => 'actionpack/mime_responds_extension' }
                    
class Rails::Configuration
  attr_accessor :extensions
  
  def extensions
    Array(@extensions).map(&:to_s)
  end
end

class Rails::Initializer  
  def load_application_initializers_with_railsex
    load_application_initializers_without_railsex
    
    unless configuration.extensions.to_set.subset?(RAILSEX_MAPPING.keys.to_set)
      raise "Invalid Extension Supplied. Valid extensions are : #{RAILSEX_MAPPING.keys.join(', ')}"
    end
    
    configuration.extensions.each do |extn|
      RAILS_DEFAULT_LOGGER.info "[Railsex] Loading #{extn}"
      require RAILSEX_MAPPING[extn]
    end
  end
  alias_method_chain :load_application_initializers, :railsex
end
