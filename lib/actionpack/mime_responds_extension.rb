module ActionController #:nodoc:
  
  module MimeRespondsExtension #:nodoc:
    def respond_to(*types, &block)
      raise ArgumentError, "respond_to takes either types or a block, never both" unless types.any? ^ block
      block ||= lambda { |responder| types.each { |type| responder.send(type) } }
      arity = (block.arity == 1) 
      responder = ActionController::MimeResponds::Responder.new(self, arity) 
      arity ? block.call(responder) : responder.instance_eval(&block)
      responder.respond
    end
  end
  
  module ResponderExtension #:nodoc:
    def self.included(base)
      base.alias_method_chain :initialize, :arity
      base.alias_method_chain :custom, :arity
    end
      
    def initialize_with_arity(controller, arity)
      @arity = arity
      initialize_without_arity(controller)
    end
    
    def custom_with_arity(mime_type, &block)
      mime_type = mime_type.is_a?(Mime::Type) ? mime_type : Mime::Type.lookup(mime_type.to_s)

      @order << mime_type

      @responses[mime_type] = Proc.new do
        @response.template.template_format = mime_type.to_sym
        @response.content_type = mime_type.to_s
        if block_given?  
          @arity ? block.call : @controller.instance_eval(&block)
        else  
          @controller.send(:render, :action => @controller.action_name) 
        end
      end
    end
    
  end

end

ActionController::Base.send :include, ActionController::MimeRespondsExtension
ActionController::MimeResponds::Responder.send :include, ActionController::ResponderExtension
