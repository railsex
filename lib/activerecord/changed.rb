module ActiveRecord #:nodoc:
  module Changed #:nodoc:
    def self.included(base)
      base.extend ClassMethods
      base.alias_method_chain :attributes_with_quotes, :changed
      base.alias_method_chain :write_attribute, :changed
      base.alias_method_chain :save, :changed
      base.alias_method_chain :save!, :changed
      base.alias_method_chain :reload, :changed
    end
    
    module ClassMethods
      # 
      # Specify attributes you want to be considired for partial updates.
      # 
      # Consider following model :
      # 
      #   class Person < ActiveRecord::Base
      #     lazy_attributes :history, :bio
      #   end
      # 
      # Here, the columns 'history' and 'bio' will be included in UPDATE query only if their value has been changed.
      # 
      # Notice that 'history' and 'bio' column are not present in following UPDATE query.
      #   >> p = Person.find(:first)
      #   >> p.save
      #   UPDATE people SET "created_at" = '2007-12-20 11:37:23', "name" = 'hello', "age" = 20, "address" = 'whatever', "updated_at" = '2007-12-20 22:33:48' WHERE "id" = 3
      # 
      # Now, if you change 'bio' field :
      # 
      #   >> p.bio = "SOS!"
      #   >> p.save
      #   UPDATE people SET "created_at" = '2007-12-20 11:37:23', "name" = 'hello', "bio" = 'SOS!', "age" = 20, "address" = 'whatever', "updated_at" = '2007-12-20 22:38:08' WHERE "id" = 3  
      # 
      def lazy_attributes(*args)
        @@lazy_attributes ||= Array(args).map(&:to_s).to_set
      end
    end
    
    private
    
    def attributes_with_quotes_with_changed(include_primary_key = true, include_readonly_attributes = true)
      original = attributes_with_quotes_without_changed(include_primary_key, include_readonly_attributes)
      
      # Reject only if both supplied arguments are false
      original.reject! { |key, value| changed_lazy.include?(key) } if !(include_primary_key || include_readonly_attributes)
      original
    end
    
    def write_attribute_with_changed(attr_name, value)
      changed_lazy.delete(attr_name.to_s)
      write_attribute_without_changed(attr_name, value)
    end
    
    def reload_with_changed
      @changed_lazy = nil
      reload_without_changed
    end
    
    def changed_lazy
      @changed_lazy ||= self.class.lazy_attributes.clone
    end
    
    def save_with_changed
      save_without_changed ensure @changed_lazy = nil
    end

    def save_with_changed!
      save_without_changed! ensure @changed_lazy = nil
    end
  end
end

ActiveRecord::Base.send :include, ActiveRecord::Changed