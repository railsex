# Print queries to your script/console sessions
def log_to(stream=$stdout)
  ActiveRecord::Base.logger = Logger.new(stream)
  ActiveRecord::Base.clear_active_connections!
end

class ActiveRecord::Base
  # Alias for find()
  def self.[](*options)
    self.find(*options)
  end
end
