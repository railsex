class Person < ActiveRecord::Base
  lazy_attributes :history, :bio
  validates_presence_of :name, :address, :age
end
