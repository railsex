RAILS_GEM_VERSION = '2.0.2' unless defined? RAILS_GEM_VERSION

require File.join(File.dirname(__FILE__), 'boot')

Rails::Initializer.run do |config|
  config.action_controller.session = {
    :session_key => '_test_session',
    :secret      => '5b230be6567e2558f51e2ce60441b836d2621d37a4f868036a1817ec941f2e773965df720f7516027c3f729ca6321b37e42160307519f5592b7e283fb16f0d44'
  }
  config.after_initialize do
    config.extensions = [:respond_to, :lazy_attributes, :console]
  end
end