class AddStuffToPerson < ActiveRecord::Migration
  def self.up
    add_column :people, :bio, :text
    add_column :people, :history, :text
  end

  def self.down
    remove_column :people, :history
    remove_column :people, :bio
  end
end
