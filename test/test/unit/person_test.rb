require File.dirname(__FILE__) + '/../test_helper'

class PersonTest < ActiveSupport::TestCase
  def setup
    super
    @person = Person.find :first
    @lazy = ['history', 'bio'].to_set
    @columns = Person.column_names.to_set - ['id']
  end
  
  def test_lazy_attributes_values
    assert_equal @lazy, @person.class.lazy_attributes
  end
  
  def test_quoted_attributes_should_include_all_columns
    assert_equal @columns, @person.send(:attributes_with_quotes).keys.to_set.delete('id')
  end
  
  def test_quoted_attributes_should_exclude_lazy_columns_for_updates
    assert_equal @columns - @lazy, attr_for_update(@person)
  end
  
  def test_changed_lazy_attributes_behave_well
    @person.name = "Whatever"
    assert_equal @columns - @lazy, attr_for_update(@person)
    
    @person.bio  = "Hello world"
    assert_equal @columns - ['history'], attr_for_update(@person)
    
    @person.history = "Nothing."
    assert_equal @columns, attr_for_update(@person)
    
    assert @person.save!
    @person.reload
    
    assert_equal "Nothing.", @person.history
    assert_equal "Whatever", @person.name
  end
  
  def test_save_clears_lazy_data
    @person.bio  = "DSL"
    assert_equal @columns - ['history'], attr_for_update(@person)
    
    assert @person.save
    assert_equal @columns - @lazy, attr_for_update(@person)
    assert_equal "DSL", @person.bio
  end
  
  def test_reload_clears_lazy_data
    @person.bio  = "Shenanigans"
    @person.history = "Bloody"
    assert_equal @columns, attr_for_update(@person)
    
    assert @person.reload
    
    assert_equal @columns - @lazy, attr_for_update(@person)
  end
  
  private
  
  def attr_for_update(person)
    person.send(:attributes_with_quotes, false, false).keys.to_set
  end
end
